# README #

### What is this repository for? ###

This repository stores all of the source code of the gymnastics competition 
app program.

========================================================================
    PYTHON APPLICATION : Gym Project Overview
========================================================================
UI Files:
These are the GUI screen configurations for the Gym Project. They include
the details of each screen, such as buttons, check boxes, and labels.
They also handle the colors, font sizes, and other formattings. They are
created and formatted by using the Python's PyQt Designer.

begin.ui:
This is the first screen of the program. This inputs the gymnast's 
identification number.

stop record.ui:
This is the second screen of the program. This starts and stops the video 
recording. Once the judge presses the green "start recording" button, the 
button changes to a red "stop recording" button. There is also a counter 
that tracks the number of times the judge presses the pedal during the 
routine (deduction counter). The judge judges the routine by hand (using 
pen and paper) as he watches. The next three screens accept inputs from 
the information he wrote down on his paper.

difficulty.ui:
This is the third screen of the program. This inputs the skill values. The 
judge can use the up and down arrows to adjust the number of each level (A, 
B, and C) skills the gymnast performed.

category.ui:
This is the fourth screen of the program. This inputs the element groups the 
gymnast performed. The judge can check the boxes of the element groups 
performed.

deduction.ui:
This is the fifth screen of the program. This inputs the deduction value for 
each time-stamped deduction. The number in the upper left corner tells the 
judge which number time-stamp deduction this screen is. This screen repeats 
for as many deductions as the judge recorded. After pressing the continue 
button, the video is automatically converted, and an internet browser for 
YouTube opens. The judge then must click the "allow" button in order for the 
video to be uploaded. After clicking "allow," the system automatically closes 
the browser the proceeds to the sixth screen.

final score.ui:
This is the sixth screen of the program. This displays the final score. Clicking 
"continue" moves to screen one and restarts the judging process.

main:
This is the main code of the program. It uses Python and handles the flow and 
events of the program. It contains multiple functions that control the inputs
from the Raspberry Pi, handles the scoring calculations, and uploading the
video and corresponding information directly to Youtube.